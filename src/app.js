import angular from "angular";
import uiRouter from "angular-ui-router";

var app = angular.module("app", ["templates", uiRouter]);

app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/enterprise");
  $stateProvider
    .state("insurance", {
      url: "/insurance",
      templateUrl: "app/insurance.html"
    })
    .state("enterprise", {
      url: "/enterprise",
      templateUrl: "app/enterprise.html"
    });
});
